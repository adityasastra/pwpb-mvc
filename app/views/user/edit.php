<div class="container mt-4">
    <h1>Ubah Data User</h1>
    <div class="row">
        <div class="col-6">
            <form action="<?= BASEURL ?>/user/editproses" method="POST">
                <input type="hidden" name="id" value="<?= $data['user']['id'] ?>">
                <div class="mb-3">
                    <label for="nama_penulis" class="form-label">Nama Penulis</label>
                    <input type="nama_penulis" class="form-control" id="nama_penulis" name="nama_penulis" value="<?= $data['user']['nama_penulis'] ?>">
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" value="<?= $data['user']['email'] ?>">
                </div>
                <button type=" submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>