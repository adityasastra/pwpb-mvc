<div class="container mt-3">
    <div class="row">
        <div class="col-6">
            <h3>Blog</h3>
            <button type="button" class="btn btn-primary mb-2" data-bs-toggle="modal" data-bs-target="#exampleModal">
                Tambah Tulisan
            </button>
            <ul class="list-group">
                <?php foreach ($data["blog"] as $blog) : ?>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <?= $blog['judul'] ?>
                        <div class="float-right">
                            <a href="<?= BASEURL ?>/blog/detail/<?= $blog['id'] ?>" class="badge text-bg-primary text-decoration-none">Baca</a>
                            <a href="<?= BASEURL ?>/blog/edit/<?= $blog['id'] ?>" class="badge text-bg-success text-decoration-none">Edit</a>
                            <a href="<?= BASEURL ?>/blog/hapus/<?= $blog['id'] ?>" class="badge text-bg-danger text-decoration-none">Delete</a>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah User</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="POST" action="<?= BASEURL ?>/blog/tambah">
                    <div class="mb-3">
                        <label for="penulis" class="form-label">Penulis</label>
                        <input type="hidden" value="<?= $_SESSION['user']['id'] ?>" name="penulis">
                        <input type="text" class="form-control" disabled value="<?= $_SESSION['user']['nama'] ?>">
                    </div>
                    <div class="mb-3">
                        <label for="judul" class="form-label">Judul</label>
                        <input type="text" class="form-control" id="judul" name="judul">
                    </div>
                    <div class="mb-3">
                        <label for="tulisan" class="form-label">Tulisan</label>
                        <input type="text" class="form-control" id="tulisan" name="tulisan">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Tambah User</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>