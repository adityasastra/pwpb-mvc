<?php
class User_model
{
    private $table = "user";
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function tambahUser($data)
    {

        $password = md5($data['password'] . SALT);
        $this->db->query("INSERT INTO $this->table VALUES (null, :nama_penulis, :email, :password)");
        $this->db->bind('nama_penulis', $data['nama_penulis']);
        $this->db->bind('email', $data['email']);
        $this->db->bind('password', $password);
        $this->db->execute();

        return $this->db->rowCount();
    }

    public function getAllUser()
    {
        $this->db->query("SELECT * FROM $this->table");
        return $this->db->resultSet();
    }

    public function getUserByEmail($email)
    {
        $this->db->query("SELECT * FROM $this->table WHERE email = :email");
        $this->db->bind('email', $email);
        return $this->db->single();
    }

    public function getUserById($id)
    {
        $this->db->query("SELECT * FROM $this->table WHERE id = :id");
        $this->db->bind('id', $id);
        return $this->db->single();
    }

    public function hapusUser($id)
    {
        $this->db->query("DELETE FROM $this->table WHERE id = :id");
        $this->db->bind('id', $id);
        return $this->db->execute();
    }

    public function editUser($data)
    {
        $query =  ("UPDATE $this->table SET nama_penulis = :nama_penulis, email = :email WHERE id = :id");
        $this->db->query($query);
        $this->db->bind('nama_penulis', $data['nama_penulis']);
        $this->db->bind('email', $data['email']);
        $this->db->bind('id', $data['id']);
        $this->db->execute();
    }
}
